import flask
from app.config.config import Config
from core.ControllerBase import ControllerBase
from flask import request, flash, redirect, url_for, jsonify
from app.models.gasForm import gasForm
from app.library import Mail

class SendMailController(ControllerBase):

	def send(self):
		data = flask.session['user']
		return self.setView(data=data)

	def send2(self):

		if request.method == 'POST':
			email = request.form["sendmail"]
			
		Mail.sendMail('This is my subject', Config.MAIL_SETTINGS["MAIL_USERNAME"], email, 'This is my content')
		return flask.redirect(url_for('sendmail'))

